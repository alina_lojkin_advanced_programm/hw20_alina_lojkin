﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void GenerateCards(object sender, EventArgs e)
        {
            Point nextCard = new Point();
            int i = 0;
            for (i = 0; i < 11; i++)
            {
                System.Windows.Forms.PictureBox curr = new PictureBox();
                curr.Name = "picDynamic" + i;
                curr.Image = global::WindowsFormsApp3.Properties.Resources.ace_of_diamonds;
                curr.Location = nextCard;

                curr.Click += delegate (object sender1, EventArgs e1)
                {
                    string currIn = curr.Name.Substring(curr.Name.Length - 1);
                    MessageBox.Show("You've clicked card #" + currIn);
                    ((PictureBox)sender1).Hide();
                    ((PictureBox)sender1).Dispose();
                };
                this.Controls.Add(curr);

             
            }
        }

        private string Get_msg()
        {
            byte[] bufferIn = new byte[4];
            int bytesRead = clientStream.Read(bufferIn, 0, 4);
            string input = new ASCIIEncoding().GetString(bufferIn);
            return input;
        }

        private void Send_msg()
        {
            byte[] buffer = new ASCIIEncoding().GetBytes("2000");
            clientStream.Write(buffer, 0, 4);
            clientStream.Flush();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void PictureBox7_Click(object sender, EventArgs e)
        {

        }

        private void PictureBox8_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Exit_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {

        }
    }
}
